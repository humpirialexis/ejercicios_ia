public class Main {
    public static void main(String[] args) {
        // Crear algunas entradas de juego
        EntradaJuego entrada1 = new EntradaJuego("Jugador1", 1500);
        EntradaJuego entrada2 = new EntradaJuego("Jugador2", 2000);
        EntradaJuego entrada3 = new EntradaJuego("Jugador3", 1800);

        // Crear un objeto de Puntuaciones
        Puntuaciones puntuaciones = new Puntuaciones();

        // Agregar las entradas al registro de puntuaciones
        puntuaciones.add(entrada1);
        puntuaciones.add(entrada2);
        puntuaciones.add(entrada3);

        // Mostrar las mejores puntuaciones
        System.out.println("Las mejores puntuaciones:");
        System.out.println(puntuaciones);

        // Eliminar una entrada (por ejemplo, la segunda entrada)
        System.out.println("Eliminar la segunda puntuación:");
        puntuaciones.remove(1);

        // Mostrar las mejores puntuaciones después de eliminar una entrada
        System.out.println("Las mejores puntuaciones después de eliminar:");
        System.out.println(puntuaciones);
    }
}
