/** Clase para guardar los registros en un arreglo en orden descendente. */
public class Puntuaciones {
    
    public static final int maxEntradas = 10; // Número de registros que se pueden guardar
    protected int numEntradas; // Número actual de registros
    protected EntradaJuego[] entradas; // Arreglo de registros (nombre y puntuación)

    /** Constructor por defecto */
    public Puntuaciones() {
        entradas = new EntradaJuego[maxEntradas];
        numEntradas = 0;
    }

    /** Regresa una cadena de las mayores puntuaciones */
    public String toString() {
        String s = "[";
        for (int i = 0; i < numEntradas; i++) {
            if (i > 0) s += ", "; // Separar registros con comas
            s += entradas[i];
        }
        return s + "]";
    }

    /** Intentar agregar un nuevo registro a la colección si es lo suficientemente alto */
    public void add(EntradaJuego e) {
        int nuevaPuntuacion = e.getPuntuacion();
        if (numEntradas == maxEntradas) { // El arreglo está lleno
            if (nuevaPuntuacion <= entradas[numEntradas - 1].getPuntuacion())
                return; // La nueva entrada, e, no es un registro
        } else // El arreglo no está lleno
            numEntradas++;
        // Encontrar el lugar donde la nueva entrada estará
        int i = numEntradas - 1;
        for (; (i >= 1) && (nuevaPuntuacion > entradas[i - 1].getPuntuacion()); i--)
            entradas[i] = entradas[i - 1]; // Mover entrada i un lugar a la derecha
        entradas[i] = e; // Agregar el nuevo registro a las entradas
    }

    /** Quitar el registro del índice i y devolverlo */
    public EntradaJuego remove(int i) throws IndexOutOfBoundsException {
        if ((i < 0) || (i >= numEntradas))
            throw new IndexOutOfBoundsException(8);
        // Guardar temporalmente el objeto a ser quitado
        EntradaJuego temp = entradas[i];

        for (int j = i; j < numEntradas - 1; j++) // Contar hacia adelante desde i
            entradas[j] = entradas[j + 1]; // Mover una celda a la izquierda

        return temp; // Devolver el objeto quitado
    }
}
